#!/usr/local/bin/perl

#┌─────────────────────────────────
#│ CartForm : check.cgi - 2011/10/05
#│ copyright (c) KentWeb
#│ http://www.kent-web.com/
#└─────────────────────────────────

# モジュール宣言
use strict;
use CGI::Carp qw(fatalsToBrowser);

# 外部ファイル取り込み
require './init.cgi';
my %cf = &init;

print <<EOM;
Content-type: text/html

<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=shift_jis">
<title>Check Mode</title>
</head>
<body>
<b>Check Mode: [ $cf{version} ]</b>
<ul>
EOM

my %file = (logfile => 'データファイル', numfile => '通番ファイル');
foreach ( keys(%file) ) {
	if (-f $cf{$_}) {
		print "<li>$file{$_}パス：OK\n";
		if (-r $cf{logfile} && -w $cf{logfile}) {
			print "<li>$file{$_}パーミッション : OK\n";
		} else {
			print "<li>$file{$_}パーミッション : NG\n";
		}
	} else {
		print "<li>$file{$_}パス : NG\n";
	}
}

# メールソフトチェック
print "<li>sendmailパス：";
if (-e $cf{sendmail}) {
	print "OK\n";
} else {
	print "NG → $cf{sendmail}\n";
}

# テンプレート
my @tmpl = qw|conf.html err1.html err2.html send.html send-bank.html send-credit.html mail.txt reply.txt|;
foreach (@tmpl) {
	print "<li>テンプレート ( $_ ) : ";

	if (-f "$cf{tmpldir}/$_") {
		print "パスOK\n";
	} else {
		print "パスNG → $_\n";
	}
}

print <<EOM;
</ul>
</body>
</html>
EOM
exit;

