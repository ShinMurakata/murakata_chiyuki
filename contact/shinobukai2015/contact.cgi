#!/usr/local/bin/perl

require './jcode.pl';
$mailprog = '/usr/lib/sendmail';

$mailto = 'murakata.shinobu.kai@gmail.com';
$from = '村方千之オフィシャルWebSite[しのぶ会お問合せ]<chiyukimurakata-ws-info>';

&check ;

if 		( $mode eq "kakunin")		{ &kakunin(); }
elsif 	( $mode eq "sousin" )		{ &sousin(); }
else								{ &kakunin(); }

exit;

sub check
{
	if ($ENV{'REQUEST_METHOD'} eq "POST") {
		read(STDIN, $buffer, $ENV{'CONTENT_LENGTH'});
	}
	else {
		 $buffer = $ENV{'QUERY_STRING'};
	}

	@pairs = split(/&/, $buffer);

	foreach $pair(@pairs)
	{
		($name,$value) = split(/=/,$pair);
		$name =~ tr/+/ /;
		$name =~ s/%([a-fA-F0-9][a-fA-F0-9])/pack("C", hex($1))/eg;
		&jcode'convert(*name,'sjis');
		$value =~ tr/+/ /;
		$value =~ s/%([a-fA-F0-9][a-fA-F0-9])/pack("C", hex($1))/eg;
		&jcode'convert(*value,'sjis');
		$value =~ s/<!--(.|\n)*-->//g;
		$CONFIG{$name} = $value;
	}
	$mode = $CONFIG{'mode'} ;
}

sub kakunin
{
	$data4="$CONFIG{'d'}";		#名前
	$data8="$CONFIG{'h'}";		#メール１
	$data9="$CONFIG{'i'}";		#メール２

	$data1="$CONFIG{'a'}";		#内容
	$data31="$CONFIG{'a'}";		#内容

	$data31	=~ s/\r\n/<BR>/g;
	$data31	=~ s/\r|\n/<BR>/g;

	$error_msg = "";
	if ( $data1 eq "" || $data1 eq " " || $data1 eq "　" ){
		$error_msg = "●お問合せの内容が記入されていません。<BR>" ;
	}
	if ( ( $data5 =~ /[^0-9]/ && $data5 ne "" ) || ( $data6 =~ /[^0-9]/ && $data6 ne "" ) )
	{
		$error_msg = $error_msg."●郵便番号を正しく入力して下さい。<BR>" ;
	}
	$yubin = "" ;
	if ( $data5 || $data6 )
	{
		$yubin = $data5."-".$data6 ;
	}

	if ( ( $data8 ne "" && $data8 !~ /[0-9A-Za-z_-]+@[0-9A-Za-z.-]+[.][A-Za-z]/ )
			|| ( $data9 ne "" && $data9 !~ /[0-9A-Za-z_-]+@[0-9A-Za-z.-]+[.][A-Za-z]/ ) )
	{
		$error_msg = $error_msg."●メールアドレスを正しく入力して下さい。<BR>" ;
	}
	if ( $data8 ne "" && $data9 ne "" && $data8 ne $data9 )
	{
		$error_msg = $error_msg."●同じメールアドレスを入力して下さい。<BR>" ;
	}
	if ( $data11 =~ /[^0-9]/ && $data11 ne "" )
	{
		$error_msg = $error_msg."●内線番号を正しく入力して下さい。<BR>" ;
	}

	if ( ( $data41 =~ /[^0-9]/ && $data41 ne "" ) || ( $data42 =~ /[^0-9]/ && $data42 ne "" ) 
|| ( $data43 =~ /[^0-9]/ && $data43 ne "" ) )
	{
		$error_msg = $error_msg."●電話番号を正しく入力して下さい。<BR>" ;
	}
	if ( ( $data51 =~ /[^0-9]/ && $data51 ne "" ) || ( $data52 =~ /[^0-9]/ && $data52 ne "" )
 || ( $data53 =~ /[^0-9]/ && $data53 ne "" ) )
	{
		$error_msg = $error_msg."●ＦＡＸ番号を正しく入力して下さい。<BR>" ;
	}
	$tel = "" ;
	if ( $data41 ){$tel = $tel.$data41} ;
	if ( $data42 ){$tel = $tel."-".$data42} ;
	if ( $data43 ){$tel = $tel."-".$data43} ;
	$fax = "" ;
	if ( $data51 ){$fax = $fax.$data51} ;
	if ( $data52 ){$fax = $fax."-".$data52} ;
	if ( $data53 ){$fax = $fax."-".$data53} ;


	if ( $error_msg ne "" )
	{
		&error();
	}
	else
	{
		print "Content-type:text/html\n\n";
		print <<EOF;
		<html><head><title>内容確認</title>
		<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
		</head>
		<BODY bgcolor="#FFFFFF" text="#000000" marginwidth="0" marginheight="0" topmargin="0" leftmargin="0">
		<center>
		<br>内容は、以下でよろしいでしょうか？<br><br>
		<table cellpadding=0 cellspacing=0 border=1 width=400>
		<tr>
			<td>
			<table cellpadding=1 cellspacing=1 border=0 width=400>
			<tr><td nowrap valign=top width=150 bgcolor=#FEE6F2>お問い合わせ内容：</td><td nowrap>$data31</td></tr>
EOF
			if ( $data2 )
			{
				print "<tr><td nowrap bgcolor=#FEE6F2>ご施設名：</td><td nowrap>$data2</td></tr>\n"
			}
			if ( $data3 )
			{
				print "<tr><td nowrap bgcolor=#FEE6F2>部署名：</td><td nowrap>$data3</td></tr>\n"
			}
			if ( $data4 )
			{
				print "<tr><td nowrap bgcolor=#FEE6F2>お名前：</td><td nowrap>$data4</td></tr>\n"
			}
			if ( $yubin )
			{
				print "<tr><td nowrap bgcolor=#FEE6F2>〒：</td><td nowrap>$yubin</td></tr>\n"
			}
			if ( $data7 )
			{
				print "<tr><td nowrap bgcolor=#FEE6F2>住所：</td><td nowrap>$data7</td></tr>\n"
			}
			if ( $data8 )
			{
				print "<tr><td nowrap bgcolor=#FEE6F2>メールアドレス：</td><td nowrap>$data8</td></tr>\n"
			}
			if ( $tel )
			{
				print "<tr><td nowrap bgcolor=#FEE6F2>お電話番号：</td><td nowrap>$tel</td></tr>\n"
			}
			if ( $data11 )
			{
				print "<tr><td nowrap bgcolor=#FEE6F2>内線：</td><td nowrap>$data11</td></tr>\n"
			}
			if ( $fax )
			{
				print "<tr><td nowrap bgcolor=#FEE6F2>ＦＡＸ：</td><td nowrap>$fax</td></tr>\n"
			}
			print <<EOF;
			</table>
			</td>
		</tr>
		</table>
		<br>
		<table width=450>
		<tr>
			<td align=center>
			<form action=./contact.cgi method=post>
				<input type=hidden name=mode value=sousin>
				<input type=hidden name=a value="$data1">
				<input type=hidden name=b value="$data2">
				<input type=hidden name=c value="$data3">
				<input type=hidden name=d value="$data4">
				<input type=hidden name=e value="$yubin">
				<input type=hidden name=g value="$data7">
				<input type=hidden name=h value="$data8">
				<input type=hidden name=i value="$tel">
				<input type=hidden name=k value="$data11">
				<input type=hidden name=l value="$fax">
				<input type=submit value="　送信　" name=submit>
			</form>
			</td>
			<td>
				<form>
				<input type=button name="ok" value="　戻る　" onclick="JavaScript:history.back()">
				</form>
			</td>
		</tr>
		</table>
		</center></body>
		</html>
EOF
	}
}

sub sousin
{
	&send_mail;

	print "Content-type:text/html\n\n";
	print <<EOF;
	<html><head><title>送信完了</title>
	<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
	</head>
	<body><center><font size="4">
	<br>内容を送信しました。<br>
	<a href="http://chiyuki-murakata.com/">TOPページへ</a><br>
	</font></center></body>
	</html>
EOF

}

sub send_mail {

   	open(MAIL,"|$mailprog -t");
	$subject = "しのぶ会　お問い合わせ・メッセージ";
	&jcode'convert(*subject, 'sjis');

	print MAIL "To: $mailto\n";
	print MAIL "From: $from\n";
	print MAIL "Subject: $subject\n";
	print MAIL "\n";
	print MAIL "村方千之オフィシャルホームページをご覧のお客様より以下の問合せ・メッセージが届きました。\n";
	print MAIL "----------------------------------------------------\n";
	print MAIL "内容：$CONFIG{'a'}\n";
	print MAIL "名前：$CONFIG{'d'}\n";
	print MAIL "メール：$CONFIG{'h'}\n";
	print MAIL "----------------------------------------------------\n";

	close (MAIL);
}


sub error
{
	print "Content-type:text/html\n\n";
	print <<EOF;
	<html><head><title>入力エラー</title>
	<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
	</head>
	<body>
	$error_msg<br>
	<a href="JavaScript:history.back()">戻る</a></font>
	</body></html>
EOF
}
