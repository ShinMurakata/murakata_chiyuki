#!/usr/local/bin/perl

#┌─────────────────────────────────
#│ CartForm : cartform.cgi - 2011/10/05
#│ copyright (c) KentWeb
#│ http://www.kent-web.com/
#└─────────────────────────────────

# モジュール実行
use strict;
use CGI::Carp qw(fatalsToBrowser);
use lib './lib';
use Jcode;

# 設定ファイル認識
require "./init.cgi";
my %cf = &init;

# データ受理
my ($key,$need,$in) = &parse_form;

# 必須入力チェック
my ($check,@err,@need);
if ($$in{need} || @$need > 0) {

	# needフィールドの値を必須配列に加える
	my @tmp = split(/\s+/, $$in{need});
	push(@need,@tmp);

	# 必須配列の重複要素を排除する
	my (@uniq, %seen);
	foreach (@need) {
		push(@uniq,$_) unless $seen{$_}++;
	}

	# 必須項目の入力値をチェックする
	foreach (@uniq) {

		# フィールドの値が投げられてこないもの（ラジオボタン等）
		if (!defined($$in{$_})) {
			$check++;
			push(@$key,$_);
			push(@err,$_);

		# 入力なしの場合
		} elsif ($$in{$_} eq "") {
			$check++;
			push(@err,$_);
		}
	}
}

# 入力内容マッチ
my ($match1,$match2);
if ($$in{match}) {
	($match1,$match2) = split(/\s+/, $$in{match}, 2);

	if ($$in{$match1} ne $$in{$match2}) {
		&error("$match1と$match2の再入力内容が異なります");
	}
}

# 入力チェック確認画面
if ($check) {
	&err_check($match2);
}

# E-mail書式チェック
if ($$in{email} =~ /\,/) {
	&error("メールアドレスにコンマ ( , ) が含まれています");
}
if ($$in{email} && $$in{email} !~ /^[\w\.\-]+\@[\w\.\-]+\.[a-zA-Z]{2,}$/) {
	&error("メールアドレスの書式が不正です");
}

# --- プレビュー
if ($$in{mode} ne "send") {

	# 連続送信チェック
	&check_post('view');

	# 確認画面
	&preview;

# --- 送信実行
} else {

	# 連続送信チェック
	&check_post('send');

	# sendmail送信
	&send_mail;
}

#-----------------------------------------------------------
#  プレビュー
#-----------------------------------------------------------
sub preview {
	# 送信内容チェック
	&check_input;

	# 時間取得
	my $time = time;

	# テンプレート読込
	open(IN,"$cf{tmpldir}/conf.html") or &error("open err: conf.html");
	my $tmpl = join('', <IN>);
	close(IN);

	# テンプレート分割
	my ($head, $loop, $foot);
	if ($tmpl =~ /(.+)<!-- cell_begin -->(.+)<!-- cell_end -->(.+)/s) {
		($head, $loop, $foot) = ($1, $2, $3);
	} else {
		&error("テンプレートが不正です");
	}

	# 都道府県
	my ($pref,$dcost) = split(/,/, ${$cf{pref}}[$$in{pref}]);

	# 注文情報
	my $all = 0;
	my $order;
	foreach ( split(/\s+/, $$in{order}) ) {
		my ($item,$price) = split(/,/, $cf{order}{$_});
		$all += $price;

		$price = &comma($price);
		$order .= "$item [￥$price]<br>\n";
	}

	# 小計
	my $kei = &comma($all);
	$order .= "[小計] ￥$kei<br>\n";

	# 送料
	if ($cf{deli_nocost} > 0 && $cf{deli_nocost} <= $all) { $dcost = 0; }
	$all += $dcost;
	$dcost = &comma($dcost);
	$order .= "[送料] ￥$dcost<br>\n";

	# 手数料
	if (defined($cf{pay_cost}->{$$in{pay}})) {
		$all += $cf{pay_cost}{$$in{pay}};
		my $pcost = &comma($cf{pay_cost}{$$in{pay}});
		$order .= "[$$in{pay}手数料] ￥$pcost<br>\n";
	}

	# 合計
	$all = &comma($all);
	$order .= "[合計] ￥$all\n";
	$head =~ s/!order!/$order/;

	# お届け日
	$$in{deli} =~ s/(.+) (.+)/$1月$2日/;

	# 表示用の値を定義
	my %val;
	$val{$$in{pref}} = $pref;
	$val{'zeus-c'} = 'クレジット[連携決済]';
	$val{'zeus-b'} = '銀行振込[連携決済]';

	# 引数
	my $hidden =<<EOM;
<input type="hidden" name="mode" value="send" />
<input type="hidden" name="order" value="$$in{order}" />
EOM

	# 項目
	my ($bef, $item);
	foreach my $key (@$key) {
		next if ($bef eq $key);
		next if ($key eq "x");
		next if ($key eq "y");
		next if ($key eq "order");
		next if ($key eq "need");
		next if ($key eq "match");
		next if ($$in{match} && $key eq $match2);

		# 次画面用引数
		$hidden .= qq|<input type="hidden" name="$key" value="$$in{$key}" />\n|;

		# 改行変換
		$$in{$key} =~ s/\t/<br>/g;

		# 項目名置換
		my $tmp = $loop;
		if (defined($cf{replace}->{$key})) {
			$tmp =~ s/!key!/$cf{replace}->{$key}/;
		} else {
			$tmp =~ s/!key!/$key/;
		}
		if (defined($val{$$in{$key}})) { $$in{$key} = $val{$$in{$key}}; }
		$tmp =~ s/!val!/$$in{$key}/;
		$item .= $tmp;

		$bef = $key;
	}

	# 文字置換
	for ( $head, $foot ) {
		s/!cart_cgi!/$cf{cart_cgi}/g;
		s/<!-- hidden -->/$hidden/g;
	}

	# 画面展開
	print "Content-type: text/html\n\n";
	print $head, $item;

	# フッタ
	&footer($foot);
}

#-----------------------------------------------------------
#  送信実行
#-----------------------------------------------------------
sub send_mail {
	# 送信内容チェック
	&check_input;

	# ホスト/時間/通番/ブラウザ取得
	my $host = &get_host;
	my ($date, $mdate, $ymd) = &get_time;
	my $number = &get_number($ymd);
	my $agent = $ENV{HTTP_USER_AGENT};
	$agent =~ s/[<>&"'()+]//g;

	# 都道府県
	my ($pref,$dcost) = split(/,/, ${$cf{pref}}[$$in{pref}]);

	# 注文情報
	my ($order,$all);
	foreach ( split(/\s+/, $$in{order}) ) {
		my ($item,$price) = split(/,/, $cf{order}{$_});
		$all += $price;

		$price = &comma($price);
		$order .= "$item [￥$price]\n";
	}

	# 小計
	my $kei = &comma($all);
	$order .= "[小計] ￥$kei\n";

	# 送料
	if ($cf{deli_nocost} > 0 && $cf{deli_nocost} <= $all) { $dcost = 0; }
	$all += $dcost;
	$dcost = &comma($dcost);
	$order .= "[送料] ￥$dcost\n";

	# 手数料
	if (defined($cf{pay_cost}->{$$in{pay}})) {
		$all += $cf{pay_cost}{$$in{pay}};
		my $pcost = &comma($cf{pay_cost}{$$in{pay}});
		$order .= "[$$in{pay}手数料] ￥$pcost\n";
	}

	# 合計
	my $money = $all;
	$all = &comma($all);
	$order .= "[合計] ￥$all\n";

	# 本文テンプレ読み込み
	my $tbody;
	open(IN,"$cf{tmpldir}/mail.txt") or &error("open err: mail.txt");
	my $tbody = join('', <IN>);
	close(IN);

	# 改行
	$tbody =~ s/\r\n/\n/g;
	$tbody =~ s/\r/\n/g;

	# テンプレ変数変換
	$tbody =~ s/!date!/$date/g;
	$tbody =~ s/!agent!/$agent/g;
	$tbody =~ s/!host!/$host/g;
	$tbody =~ s/!order!/$order/g;
	$tbody =~ s/!number!/$number/g;
	Jcode::convert(\$tbody, 'jis', 'sjis');

	# 表示用の値を定義
	my %val;
	$val{$$in{pref}} = $pref;
	$val{'zeus-c'} = 'クレジット[連携決済]';
	$val{'zeus-b'} = '銀行振込[連携決済]';

	# 返信ありのとき
	my $resbody;
	if ($cf{auto_res}) {

		# テンプレ
		open(IN,"$cf{tmpldir}/reply.txt") or &error("open err: reply.txt");
		$resbody = join('', <IN>);
		close(IN);

		# 改行
		$resbody =~ s/\r\n/\n/g;
		$resbody =~ s/\r/\n/g;

		# 変数変換
		$resbody =~ s/!date!/$date/g;
		$resbody =~ s/!order!/$order/g;
		$resbody =~ s/!number!/$number/g;
		Jcode::convert(\$resbody, 'jis', 'sjis');
	}

	# 本文キー展開
	my ($bef, $mbody);
	foreach (@$key) {

		# 本文に含めない部分を排除
		next if ($_ eq "mode");
		next if ($_ eq "need");
		next if ($_ eq "match");
		next if ($_ eq "order");
		next if ($$in{match} && $_ eq $match2);
		next if ($bef eq $_);

		# name値の名前
		my $key_name;
		if ($cf{replace}->{$_}) {
			$key_name = $cf{replace}->{$_};
		} else {
			$key_name = $_;
		}

		# エスケープ
		$$in{$_} =~ s/\.\n/\. \n/g;

		# 添付ファイル風の文字列拒否
		$$in{$_} =~ s/Content-Disposition:\s*attachment;.*//ig;
		$$in{$_} =~ s/Content-Transfer-Encoding:.*//ig;
		$$in{$_} =~ s/Content-Type:\s*multipart\/mixed;\s*boundary=.*//ig;

		# 改行復元
		$$in{$_} =~ s/\t/\n/g;

		# HTMLタグ変換
		$$in{$_} =~ s/&lt;/</g;
		$$in{$_} =~ s/&gt;/>/g;
		$$in{$_} =~ s/&quot;/"/g;
		$$in{$_} =~ s/&#39;/'/g;
		$$in{$_} =~ s/&amp;/&/g;

		# 本文内容
		my $val;
		if (defined($val{$$in{$_}})) {
			$val = $val{$$in{$_}};
		} else {
			$val = $$in{$_};
		}
		my $tmp;
		if ($val =~ /\n/) {
			$tmp = "$key_name = \n$val\n";
		} else {
			$tmp = "$key_name = $val\n";
		}
		Jcode::convert(\$tmp, 'jis', 'sjis');
		$mbody .= $tmp;

		$bef = $_;
	}

	# 本文テンプレ内の変数を置き換え
	$tbody =~ s/!input!/$mbody/;

	# 返信テンプレ内の変数を置き換え
	$resbody =~ s/!input!/$mbody/ if ($cf{auto_res});

	# メールアドレスがない場合は送信先に置き換え
	my $email;
	if ($$in{email} eq "") {
		$email = $cf{mailto};
	} else {
		$email = $$in{email};
	}

	# MIMEエンコード
	my $sub_me = Jcode->new($cf{subject})->mime_encode;
	my $from;
	if ($$in{name}) {
		$$in{name} =~ s/[\r\n]//g;
		$from = Jcode->new("\"$$in{name}\" <$email>")->mime_encode;
	} else {
		$from = $email;
	}

	# --- 送信内容フォーマット開始
	# ヘッダー
	my $body = "To: $cf{mailto}\n";
	$body .= "From: $from\n";
	$body .= "Subject: $sub_me\n";
	$body .= "MIME-Version: 1.0\n";
	$body .= "Date: $mdate\n";
	$body .= "Content-Transfer-Encoding: 7bit\n";
	$body .= "X-Mailer: $cf{version}\n\n";
	$body .= "$tbody\n";

	# 返信内容フォーマット
	my $res_body;
	if ($cf{auto_res}) {
		$res_body .= "To: $email\n";
		$res_body .= "From: $cf{mailto}\n";
		$res_body .= "Subject: $sub_me\n";
		$res_body .= "MIME-Version: 1.0\n";
		$res_body .= "Content-type: text/plain; charset=iso-2022-jp\n";
		$res_body .= "Content-Transfer-Encoding: 7bit\n";
		$res_body .= "Date: $mdate\n";
		$res_body .= "X-Mailer: $cf{version}\n\n";
		$res_body .= "$resbody\n";
	}

	# senmdailコマンド
	my $scmd = $cf{sendmail};
	if ($cf{send_fcmd}) {
		$scmd .= " -f $from";
	}

	# 本文送信
	open(MAIL,"| $scmd -t -i") or &error("メール送信失敗");
	print MAIL "$body\n";
	close(MAIL);

	# 返信送信
	if ($cf{auto_res}) {
		my $scmd = $cf{sendmail};
		if ($cf{send_fcmd}) {
			$scmd .= " -f $cf{mailto}";
		}
		open(MAIL,"| $scmd -t -i") or &error("メール送信失敗");
		print MAIL "$res_body\n";
		close(MAIL);
	}

	# テンプレート定義
	my ($flg,$tmplfile);
	if ($$in{pay} eq 'zeus-c') {
		$flg++;
		$tmplfile = 'send-credit.html';
	} elsif ($$in{pay} eq 'zeus-b') {
		$flg++;
		$tmplfile = 'send-bank.html';
	} else {
		$tmplfile = 'send.html';
	}

	# テンプレート読み込み
	open(IN,"$cf{tmpldir}/$tmplfile") or &error("open err: $tmplfile");
	my $tmpl = join('', <IN>);
	close(IN);

	# 文字置換
	$tmpl =~ s/!back_url!/$cf{back_url}/g;
	if ($flg) {
		$tmpl =~ s/!zeus_num!/$cf{zeus_num}/g;
		$tmpl =~ s/!tel!/$$in{tel}/g;
		$tmpl =~ s/!email!/$$in{email}/g;
		$tmpl =~ s/!money!/$money/g;
		$tmpl =~ s/!sendid!/$number/g;
	}

	# 表示
	print "Content-type: text/html\n\n";
	&footer($tmpl);
}

#-----------------------------------------------------------
#  入力エラー表示
#-----------------------------------------------------------
sub err_check {
	my $match2 = shift;

	# テンプレート読み込み
	open(IN,"$cf{tmpldir}/err2.html") or &error("open err: err2.html");
	my $tmpl = join('', <IN>);
	close(IN);

	# テンプレート分割
	my ($head,$loop,$foot);
	if ($tmpl =~ /(.+)<!-- cell_begin -->(.+)<!-- cell_end -->(.+)/s) {
		($head,$loop,$foot) = ($1, $2, $3);
	} else {
		&error("テンプレートが不正です");
	}

	# 注文情報
	my $order;
	foreach ( split(/\s+/, $$in{order}) ) {
		my ($item,$price) = split(/,/, $cf{order}{$_});
		$order .= "$item<br>\n";
	}
	$head =~ s/!order!/$order/;

	# お届け日
	$$in{deli} =~ s/(.+) (.+)/$1月$2日/;

	# 都道府県
	my ($pref,$dcost) = split(/,/, ${$cf{pref}}[$$in{pref}]);
	$$in{pref} = $pref;

	# 画面展開
	print "Content-type: text/html\n\n";
	print $head;
	my $bef;
	foreach my $key (@$key) {
		next if ($key eq "need");
		next if ($key eq "match");
		next if ($key eq "order");
		next if ($$in{match} && $key eq $match2);
		next if ($key eq "match");
		next if ($bef eq $key);
		next if ($key eq "x");
		next if ($key eq "y");

		my $key_name = $key;
		my $tmp = $loop;
		if(defined($cf{replace}->{$key})) {
			$key_name = $cf{replace}->{$key};
		}
		$tmp =~ s/!key!/$key_name/;

		my $erflg;
		foreach my $err (@err) {
			if ($err eq $key) {
				$erflg++;
				last;
			}
		}
		# 入力なし
		if ($erflg) {
			$tmp =~ s/!val!/<span class="msg">$key_nameは入力必須です.<\/span>/;

		# 正常
		} else {
			$$in{$key} =~ s/\t/<br>/g;
			$tmp =~ s/!val!/$$in{$key}/;
		}
		print $tmp;

		$bef = $key;
	}
	print $foot;
	exit;
}

#-----------------------------------------------------------
#  フォームデコード
#-----------------------------------------------------------
sub parse_form {
	my ($buf,@key,@need,%in);
	if ($ENV{REQUEST_METHOD} eq "POST") {
		&error('受理できません') if ($ENV{CONTENT_LENGTH} > $cf{maxdata});
		read(STDIN, $buf, $ENV{CONTENT_LENGTH});
	} else {
		$buf = $ENV{QUERY_STRING};
	}
	foreach ( split(/&/, $buf) ) {
		my ($key,$val) = split(/=/);
		$key =~ tr/+/ /;
		$key =~ s/%([a-fA-F0-9][a-fA-F0-9])/pack("H2", $1)/eg;
		$val =~ tr/+/ /;
		$val =~ s/%([a-fA-F0-9][a-fA-F0-9])/pack("H2", $1)/eg;

		# 無効化
		$key =~ s/[<>&"'\r\n]//g;
		$val =~ s/&/&amp;/g;
		$val =~ s/</&lt;/g;
		$val =~ s/>/&gt;/g;
		$val =~ s/"/&quot;/g;
		$val =~ s/'/&#39;/g;
		$val =~ s/\r\n/\t/g;
		$val =~ s/\n/\t/g;
		$val =~ s/\r/\t/g;

		# 入力必須
		if ($key =~ /^_(.+)/) {
			$key = $1;
			push(@need,$key);
		}

		# 受け取るキーの順番を覚えておく
		push(@key,$key);

		$in{$key} .= " " if (defined($in{$key}));
		$in{$key} .= $val;
	}
	# リファレンスで返す
	return (\@key, \@need, \%in);
}

#-----------------------------------------------------------
#  エラー処理
#-----------------------------------------------------------
sub error {
	my $err = shift;

	open(IN,"$cf{tmpldir}/err1.html") or die;
	print "Content-type: text/html\n\n";
	while (<IN>) {
		s/!error!/$err/;

		print;
	}
	close(IN);

	exit;
}

#-----------------------------------------------------------
#  フッター
#-----------------------------------------------------------
sub footer {
	my $foot = shift;

	# 著作権表記（削除不可）
	my $copy = <<EOM;
<p style="text-align:center;font-size:10px;font-family:Verdana,Helvetica,Arial;margin-top:3em;">
- <a href="http://www.kent-web.com/" target="_top">CartForm</a> -
</p>
EOM

	if ($foot =~ /(.+)(<\/body[^>]*>.*)/si) {
		print "$1$copy$2\n";
	} else {
		print "$foot$copy\n";
		print "</body></html>\n";
	}
	exit;
}

#-----------------------------------------------------------
#  時間取得
#-----------------------------------------------------------
sub get_time {
	$ENV{TZ} = "JST-9";
	my ($sec,$min,$hour,$mday,$mon,$year,$wday) = localtime(time);
	my @week  = qw|Sun Mon Tue Wed Thu Fri Sat|;
	my @month = qw|Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec|;

	# 日時フォーマット
	my $date  = sprintf("%04d/%02d/%02d(%s) %02d:%02d:%02d",
			$year+1900,$mon+1,$mday,$week[$wday],$hour,$min,$sec);
	my $mdate = sprintf("%s, %02d %s %04d %02d:%02d:%02d",
			$week[$wday],$mday,$month[$mon],$year+1900,$hour,$min,$sec) . " +0900";
	my $ymd   = sprintf("%04d%02d%02d", $year+1900,$mon+1,$mday);

	return ($date,$mdate,$ymd);
}

#-----------------------------------------------------------
#  ホスト名取得
#-----------------------------------------------------------
sub get_host {
	# ホスト/IP取得
	my $host = $ENV{REMOTE_HOST};
	my $addr = $ENV{REMOTE_ADDR};

	# ホスト未取得の場合gethostbyaddr関数で取得
	if ($cf{gethostbyaddr} && ($host eq "" || $host eq $addr)) {
		$host = gethostbyaddr(pack("C4", split(/\./, $addr)), 2);
	}
	$host ||= $addr;

	return $host;
}

#-----------------------------------------------------------
#  送信チェック
#-----------------------------------------------------------
sub check_post {
	my $job = shift;

	# 時間/IP取得
	my $now  = time;
	my $addr = $ENV{REMOTE_ADDR};

	# ログオープン
	open(DAT,"+< $cf{logfile}") or &error("open err: $cf{logfile}");
	eval "flock(DAT, 2);";
	my $data = <DAT>;

	# 分解
	my ($ip, $time) = split(/<>/, $data);

	# IP/時間をチェック
	if ($ip eq $addr && $now - $time <= $cf{block_post}) {
		close(DAT);
		&error("連続送信は$cf{block_post}秒間お待ちください");
	}

	# 送信時は保存
	if ($job eq "send") {
		seek(DAT, 0, 0);
		print DAT "$addr<>$now";
		truncate(DAT, tell(DAT));
	}
	close(DAT);
}

#-----------------------------------------------------------
#  桁区切り
#-----------------------------------------------------------
sub comma {
	local($_) = @_;

	1 while s/(.*\d)(\d\d\d)/$1,$2/;
	$_;
}

#-----------------------------------------------------------
#  通番取得
#-----------------------------------------------------------
sub get_number {
	my $ymd = shift;

	# 通番取得
	open(NUM,"+< $cf{numfile}") or &error("open err: $cf{numfile}");
	eval "flock(NUM, 2);";
	my $num = <NUM> + 1;
	seek(NUM, 0, 0);
	print NUM $num;
	truncate(NUM, tell(NUM));
	close(NUM);

	# 通番を返す [ 日付 + 4桁連番 ]
	return $ymd . sprintf("%04d", $num);
}

#-----------------------------------------------------------
#  入力チェック
#-----------------------------------------------------------
sub check_input {
	# post送信チェック
	if ($cf{postonly} && $ENV{REQUEST_METHOD} ne 'POST') {
		&error("不正なアクセスです");
	}

	# データチェック
	&error("データを取得できません") if (@$key == 0);

	# 注文品チェック
	&error("注文品が未選択です") if ($$in{order} eq '');

	# 支払形態チェック
	if ($cf{zeus_serv} == 0 && $$in{pay} =~ /^zeus-[cb]$/) {
		&error("連携決済は選択できません。ゼウスとの契約が必要です。");
	} elsif ($cf{zeus_serv} == 1 && $$in{pay} eq 'zeusu-b') {
		&error("銀行振込連携決済は選択できません");
	}
}

