# モジュール宣言/変数初期化
use strict;
my %cf;
#┌─────────────────────────────────
#│ CartForm : cartform.cgi - 2011/10/05
#│ copyright (c) KentWeb
#│ http://www.kent-web.com/
#└─────────────────────────────────
$cf{version} = 'CartForm v2.0';
#┌─────────────────────────────────
#│ [注意事項]
#│ 1. このスクリプトはフリーソフトです。このスクリプトを使用した
#│    いかなる損害に対して作者は一切の責任を負いません。
#│ 2. 設置に関する質問はサポート掲示板にお願いいたします。
#│    直接メールによる質問はお受けいたしておりません。
#└─────────────────────────────────

#===========================================================
#  ▼基本設定
#===========================================================

# 送信先メールアドレス
$cf{mailto} = 'cd2011@chiyuki-murakata.com';

# sendmailのパス【サーバパス】
# → プロバイダの指定を確認のこと
$cf{sendmail} = '/usr/lib/sendmail';

# sendmailへの-fコマンド（プロバイダの仕様確認）
# 0=no 1=yes
$cf{send_fcmd} = 0;

# フォームのname値の置き換えをする場合
# → 英字のname値を日本語に自動的に置き換えます。
# 例: 「email = xx@xx.xx」→「メールアドレス = xx@xx.xx」
$cf{replace} = {
	'name'  => 'お名前',
	'email' => 'メールアドレス',
	'pay'   => '支払方法',
	'deli'  => 'お届け月日',
	'time'  => 'お届け時間',
	'tel'   => '電話番号',
	'zip'   => '郵便番号',
	'pref'  => '都道府県',
	'addr'  => '住所',
	'memo'  => '備考',

	};

# 送信者へのメール返信
# 0=no 1=yes
$cf{auto_res} = 1;

# メールタイトル
$cf{subject} = 'ご注文フォーム';

# 返信向けメールタイトル
$cf{sub_reply} = 'ご注文内容の確認';

# 本体プログラム【URLパス】
$cf{cart_cgi} = './cartform.cgi';

# ログファイル【サーバパス】
$cf{logfile} = './data/log.cgi';

# 通番ファイル【サーバパス】
$cf{numfile} = './data/num.dat';

# テンプレートディレクトリ【サーバパス】
$cf{tmpldir} = './tmpl';

# 商品情報
# → 左（添え字）は「商品コード」
# → 右（値）は「商品名＋価格」をコンマ区切りで指定
$cf{order} = {
	'0001' => '村方千之の世界 II,3500',
	'0002' => 'ヴィラ＝ロボス 室内オーケストラの世界,3000',
	'0003' => '追加発送費用,300',
	};

# 都道府県
# → 県別に送料を指定する時はコンマの後に送料を指定
# → 送料が不要な場合は送料部分を 0 とする
$cf{pref} = [
	'',
	'北海道,200',
	'青森県,200',
	'岩手県,200',
	'宮城県,200',
	'秋田県,200',
	'山形県,200',
	'福島県,200',
	'茨城県,200',
	'栃木県,200',
	'群馬県,200',
	'埼玉県,200',
	'千葉県,200',
	'東京都,200',
	'神奈川県,200',
	'新潟県,200',
	'富山県,200',
	'石川県,200',
	'福井県,200',
	'山梨県,200',
	'長野県,200',
	'岐阜県,200',
	'静岡県,200',
	'愛知県,200',
	'三重県,200',
	'滋賀県,200',
	'京都府,200',
	'大阪府,200',
	'兵庫県,200',
	'奈良県,200',
	'和歌山県,200',
	'鳥取県,200',
	'島根県,200',
	'岡山県,200',
	'広島県,200',
	'山口県,200',
	'徳島県,200',
	'香川県,200',
	'愛媛県,200',
	'高知県,200',
	'福岡県,200',
	'佐賀県,200',
	'長崎県,200',
	'熊本県,200',
	'大分県,200',
	'宮崎県,200',
	'鹿児島県,200',
	'沖縄県,200',
	];

# 送料の無料サービス
# → 一定金額以上で送料を無料にする場合、数字を入力
# → この機能を使用しない場合は「0」とする
$cf{deli_nocost} = 0;

# 支払形態で手数料付加
# [例] 代引のときは+500円
# → 左（添え字）はname値「pay」の値を指定
# → 右（値）は手数料
$cf{pay_cost} = {
	'代引' => 500,
	};

# 送信後の戻り先【URLパス】
$cf{back_url} = '../index.html';

# 最大受信サイズ（Byte）
# → 例 : 102400Bytes = 100KB
$cf{maxdata} = 102400;

# 同一IPアドレスからの連続送信制御
# → 許可する間隔を秒数で指定（0にするとこの機能は無効）
$cf{block_post} = 0;

# 送信は method=POST 限定 (セキュリティ対策)
# 0=no 1=yes
$cf{postonly} = 1;

# ホスト名取得方法
# 0 : gethostbyaddr関数を使わない
# 1 : gethostbyaddr関数を使う
$cf{gethostbyaddr} = 0;

# --- [ ここより下はゼウス (ZEUS) クレジット決済の設定 ]
#
# [ ゼウスサービスを利用する ]
#  0 : しない
#  1 : クレジットサービスのみ利用【ゼウス社との契約が必要】
#  2 : クレジットと銀行決済サービスを利用【ゼウス社との契約が必要】
$cf{zeus_serv} = 0;

# ゼウス契約NO (IPコード)
$cf{zeus_num} = '99999';

#===========================================================
#  ▲設定完了
#===========================================================

# 設定値を返す
sub init {
	return %cf;
}


1;

